<?php

/**
 * Form build callback for Views substitution settings.
 */
function viewssub_settings_form() {
  $custom_substitutions = viewssub_get_custom_substitutions();
  $keys = array();
  $values = array();

  // Split keys and values in separate arrays, so we can implode them in each
  // textarea later.
  foreach ($custom_substitutions as $key => $value) {
    $keys[] = $key;
    $values[] = $value;
  }

  $form = array();
  $form['settings'] = array(
    '#prefix' => '<div class="admin clear-block">',
    '#suffix' => '</div>',
  );
  $form['settings']['keys'] = array(
    '#type' => 'textarea',
    '#title' => t('Keys'),
    '#description' => t('Enter one key per row. Example: <em>***CATEGORY_VID***</em>.'),
    '#default_value' => implode("\r\n", $keys),
    '#prefix' => '<div class="left clear-block">',
    '#suffix' => '</div>',
  );
  $form['settings']['values'] = array(
    '#type' => 'textarea',
    '#title' => t('Values'),
    '#description' => t('Enter one value per row, Example: <em>9</em>.'),
    '#default_value' => implode("\r\n", $values),
    '#prefix' => '<div class="right clear-block">',
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Form validation callback for Views substitution settings.
 */
function viewssub_settings_form_validate($form, $form_state) {
  $keys = explode("\r\n", $form_state['values']['keys']);
  $values = explode("\r\n", $form_state['values']['values']);

  foreach ($keys as $i => $key) {
    // Check for a corresponding value.
    if (!isset($values[$i])) {
      form_set_error('values', t("All keys doesn't have corresponding values"));
      return;
    }
  }
}

/**
 * Form submit callback for Views substitution settings.
 */
function viewssub_settings_form_submit($form, $form_state) {
  $custom_substitutions = array();
  $keys = explode("\r\n", $form_state['values']['keys']);
  $values = explode("\r\n", $form_state['values']['values']);

  foreach ($keys as $i => $key) {
    $custom_substitutions[$key] = $values[$i];
  }
  viewssub_set_custom_substitutions($custom_substitutions);
}
