<?php

/**
 * Implementation of hook_views_query_substitutions().
 */
function viewssub_views_query_substitutions($view) {
  return viewssub_get_custom_substitutions();
}

/**
 * Implementation of hook_views_handlers().
 */
function viewssub_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'viewssub') . '/handlers',
    ),
    'handlers' => array(
      'viewssub_handler_filter_in_operator' => array(
	'parent' => 'views_handler_filter_in_operator',
      ),
      'viewssub_handler_filter_many_to_one' => array(
	'parent' => 'views_handler_filter_many_to_one',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
function viewssub_views_data_alter(&$data) {
  $data['node']['viewssub_type'] = array(
    'title' => t('Substituted type'),
    'real field' => 'type',
    'filter' => array(
      'handler' => 'viewssub_handler_filter_in_operator',
    ),
  );
  $data['term_data']['viewssub_vid'] = array(
    'title' => t('Substituted vocabulary'),
    'real field' => 'vid',
    'filter' => array(
      'handler' => 'viewssub_handler_filter_in_operator',
    ),
  );
  $data['users_roles']['viewssub_rid'] = array(
    'title' => t('Substituted roles'),
    'real field' => 'rid',
    'filter' => array(
      'handler' => 'viewssub_handler_filter_many_to_one',
    ),
  );
}
