<?php

class viewssub_handler_filter_many_to_one extends views_handler_filter_many_to_one {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }
    $this->value_options = viewssub_get_substitution_options($this);
  }
}
