<?php

/**
 * Implementation of hook_menu().
 */
function viewssub_menu() {
  return array(
    'admin/settings/viewssub' => array(
      'title' => 'Custom Views query substitutions',
      'description' => 'Define custom Views query substitutions.',
      'access arguments' => array('administer site configuration'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('viewssub_settings_form'),
      'type' => MENU_NORMAL_ITEM,
      'file' => 'viewssub.admin.inc',
    ),
  );
}

/**
 * Implementation of hook_help().
 */
function viewssub_help($path, $arg) {
  if ($path == 'admin/settings/viewssub') {
    return
      '<p>' . t('Enter one key and one value per line. The keys you enter will be substituted with the corresponding values in all Views queries. It\'s also possible to define substitutions by implementing <code>hook_views_query_substitutions()</code>.') . '</p>' .
      '<p>' . t('To make use of custom defined substitutions (or other substitutions defined via <code>hook_views_query_substitutions()</code>), you need to build your views with the appropriate substitution handlers provided by this module.') . '</p>';
  }
}

/**
 * Implementation of hook_views_api().
 */
function viewssub_views_api() {
  return array(
    'api' => 2,
  );
}

/**
 * Get all Views substitutions.
 *
 * A helper function to fetch all Views substitutions in a format that can be
 * used in a form as select or radio options.
 */
function viewssub_get_substitution_options($view) {
  $options = array();
  $substitutions = module_invoke_all('views_query_substitutions', $view);

  foreach ($substitutions as $key => $value) {
    $options[$key] = $key;
  }
  return $options;
}

/**
 * Get custom Views substitutions.
 */
function viewssub_get_custom_substitutions() {
  $custom_substitutions = array();
  $keys = variable_get('viewssub_custom_keys', array());
  $values = variable_get('viewssub_custom_values', array());

  foreach ($keys as $i => $key) {
    // Check for a corresponding value.
    if (isset($values[$i])) {
      $custom_substitutions[$key] = $values[$i];
    }
  }
  return $custom_substitutions;
}

/**
 * Set custom Views substitutions.
 */
function viewssub_set_custom_substitutions($custom_substitutions) {
  $keys = array();
  $values = array();

  // Split keys and values into separate arrays. We do this because of the way
  // we store keys and values. We could have used separate variables for every
  // key/value pair. But I don't like user defined variables in the variable
  // table because they are hard to query.
  foreach ($custom_substitutions as $key => $value) {
    $keys[] = $key;
    $values[] = $value;
  }
  variable_set('viewssub_custom_keys', $keys);
  variable_set('viewssub_custom_values', $values);
}
